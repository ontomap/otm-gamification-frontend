// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // OTM_INSTANCE: 'https://dev.co3.ontomap.eu',
  OTM_INSTANCE: 'http://localhost:9000',
  FL_INSTANCE: 'https://api.co3-dev.firstlife.org',
  FL_DETAILS: 'https://co3.firstlife.org/details/',
  FL_STORAGE: 'https://storage.beta.firstlife.org',
  FIRSTLIFE_IMAGE_TAG: 'logo-badge',
  USER_AUTH: {
    access_token: '8iuRw9xCPKwpBUgk',
    auth_server: 'CO3UUM_DEV',
    member_id: '92@CO3UUM_DEV'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
