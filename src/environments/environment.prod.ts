export const environment = {
  production: true,
  OTM_INSTANCE: 'https://dev.co3.ontomap.eu',
  FL_INSTANCE: 'https://api.co3-dev.firstlife.org',
  FL_STORAGE: 'https://storage.beta.firstlife.org',
  FIRSTLIFE_IMAGE_TAG: 'logo-badge',
  USER_AUTH: {}
};
