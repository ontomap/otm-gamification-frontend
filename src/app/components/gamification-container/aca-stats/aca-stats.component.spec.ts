import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcaStatsComponent } from './aca-stats.component';

describe('AcaStatsComponent', () => {
  let component: AcaStatsComponent;
  let fixture: ComponentFixture<AcaStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcaStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcaStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
