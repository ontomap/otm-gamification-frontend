import {Component, OnInit} from '@angular/core';
import {AcaService} from '../../../services/aca.service';
import {AcaProfileService} from '../../../services/aca-profile.service';
import {ACA} from '../../../model/aca';
import {AcaProfile, TemporalVariableContainer} from '../../../model/aca-profile';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-aca-stats',
  templateUrl: './aca-stats.component.html',
  styleUrls: ['./aca-stats.component.css'],
  providers: [DatePipe]
})
export class AcaStatsComponent implements OnInit {
  acaList: ACA[] = [];
  selectedACA = 'https://api.co3-dev.firstlife.org/v6/fl/Things/5eb17fec6bd1ca539013a507';
  acaProfile: AcaProfile;
  view: any[] = [700, 300];
  timeframe = 'MONTH';

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Month';
  yAxisLabel: string = 'Variables';
  timeline: boolean = true;
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };
  data = [];
  // multi = [
  //   {
  //     'name': 'Germany',
  //     'series': [
  //       {
  //         'name': '1990',
  //         'value': 62000000
  //       },
  //       {
  //         'name': '2010',
  //         'value': 73000000
  //       },
  //       {
  //         'name': '2011',
  //         'value': 89400000
  //       }
  //     ]
  //   },
  //
  //   {
  //     'name': 'USA',
  //     'series': [
  //       {
  //         'name': '1990',
  //         'value': 250000000
  //       },
  //       {
  //         'name': '2010',
  //         'value': 309000000
  //       },
  //       {
  //         'name': '2011',
  //         'value': 311000000
  //       }
  //     ]
  //   },
  //
  //   {
  //     'name': 'France',
  //     'series': [
  //       {
  //         'name': '1990',
  //         'value': 58000000
  //       },
  //       {
  //         'name': '2010',
  //         'value': 50000020
  //       },
  //       {
  //         'name': '2011',
  //         'value': 58000000
  //       }
  //     ]
  //   },
  //   {
  //     'name': 'UK',
  //     'series': [
  //       {
  //         'name': '1990',
  //         'value': 57000000
  //       },
  //       {
  //         'name': '2010',
  //         'value': 62000000
  //       }
  //     ]
  //   }
  // ];


  constructor(private acaService: AcaService, private acaProfileService: AcaProfileService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.acaService.getAcas().subscribe(data => {
      this.acaList = data;
    });
    this.loadAcaProfile();
  }

  loadAcaProfile(): void {
    if (this.selectedACA) {
      this.acaProfileService.getAcaProfile(this.selectedACA).subscribe(profile => {
        if (profile) {
          this.acaProfile = profile;
          console.log(profile);
          this.buildData();
        }
      });
    }
  }

  buildData(): void {
    const containers = this.acaProfile.temporalVariableContainers;
    const variables = {};
    this.data = [];
    containers.filter(container => container.timeframe === this.timeframe)
      .forEach(container => {
        const data = this.getIntervalLabel(container.start, container.end);
        Object.keys(container.variables).forEach(v => {
          if (!(v in variables)) {
            variables[v] = [];
          }

          variables[v].push({
            start: container.start,
            name: data,
            value: container.variables[v]
          });
        });
      });
    const tempData = [];
    Object.keys(variables).forEach(v => {
      tempData.push({
        name: v,
        series: variables[v].sort((a, b) => a.start - b.start)
      });
    });
    Object.assign(this.data, tempData);
    console.log(this.data);
  }

  getIntervalLabel(start: Date, end: Date): string {
    const date = new Date(start.getTime() + (end.getTime() - start.getTime()));
    return this.timeframe === 'MONTH' ? this.datePipe.transform(date, 'LLLL') : this.datePipe.transform(date, 'w');
  }


  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

}
