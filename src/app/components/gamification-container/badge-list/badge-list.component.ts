import {Component, OnInit} from '@angular/core';
import {BadgeService} from '../../../services/badge.service';
import {Badge, BadgeStep} from '../../../model/badge';
import {
  AcaBadgeContainer,
  BadgeContainer,
  IndividualBadgeContainer,
  RankingBadgeContainer,
  TemporalBadgeContainer
} from '../../../model/badge-container';
import {MatSelectChange} from '@angular/material/select';
import {Rule} from '../../../model/rule';
import {MatDialog} from '@angular/material/dialog';
import {NewBadgeDialogComponent} from './new-badge-dialog/new-badge-dialog.component';

@Component({
  selector: 'app-badge-list',
  templateUrl: './badge-list.component.html',
  styleUrls: ['./badge-list.component.scss']
})
export class BadgeListComponent implements OnInit {
  badgeContainerList: BadgeContainer<Badge>[];
  badgeContainerFiltered: BadgeContainer<Badge>[];
  badgeList: Badge[];
  listVariable: string[];
  timeframes: string[] = ['Ever', 'Weekly', 'Monthly'];
  badgeType: string[] = ['Individual', 'ACA', 'Ranking'];
  timeFrameSelected: string;
  badgeTypeSelected: string;
  variableSelected: string;


  constructor(private badgeService: BadgeService, public dialog: MatDialog) {
    badgeService.getBadges()
      .subscribe(data => {
        this.badgeList = [];
        this.badgeContainerList = data;
        data.forEach(badgeContainer => {
          const color = this.getRandomColor();
          badgeContainer.color = color;
          badgeContainer.getBadgeList().forEach(badge => {
            badge.badgeContainer = badgeContainer;
            this.badgeList.push(badge);
          });
        });
        this.calcVariables();
        this.badgeContainerFiltered = this.badgeContainerList;
      });
  }

  ngOnInit(): void {
  }

  calcVariables(): void {
    const vars = {};
    this.badgeContainerList.forEach(badgeContainer => {
      if (badgeContainer instanceof RankingBadgeContainer) {
        vars[badgeContainer.variable] = true;
      } else {
        badgeContainer.getBadgeList().forEach(badge => {
          const badgeStep = badge as BadgeStep;
          badgeStep.preconditions.variables.forEach(variable => {
            vars[variable.variable] = true;
          });
        });
      }
    });
    this.listVariable = Object.keys(vars).sort();
    // this.listVariable.splice(0, 0, 'Any');
  }

  getRandomColor(): string {
    const h = this.rand(1, 360);
    const s = this.rand(30, 40);
    const l = this.rand(60, 80);
    return 'hsl(' + h + ',' + s + '%,' + l + '%)';
  }

  rand(min, max): number {
    return min + Math.random() * (max - min);
  }

  showBadgeContainer(badgeContainer: BadgeContainer<any>): boolean {
    let see = true;
    see = !this.badgeTypeSelected || this.badgeTypeSelected.toLowerCase() === badgeContainer.getType().toLowerCase();
    if (badgeContainer instanceof TemporalBadgeContainer) {
      see = see && (!this.timeFrameSelected ||
        (this.timeFrameSelected === 'Weekly' && badgeContainer.timeframe === 'WEEK') ||
        (this.timeFrameSelected === 'Monthly' && badgeContainer.timeframe === 'MONTH') ||
        (this.timeFrameSelected === 'Ever' && !badgeContainer.timeframe));
    }
    if (badgeContainer instanceof RankingBadgeContainer) {
      see = see && (!this.variableSelected || badgeContainer.variable === this.variableSelected);
    } else {
      badgeContainer.getBadgeList().forEach(badge => {
        if (badge.preconditions) {
          badge.preconditions.variables.forEach(variable => {
            see = see && (!this.variableSelected || variable.variable === this.variableSelected);
          });
        }
      });
    }
    return see;
  }

  openNewBadgeDialog(): void {
    const dialogRef = this.dialog.open(NewBadgeDialogComponent, {
      data: {
        badgeList: this.badgeList,
        listVariable: this.listVariable
      }
    });
  }

  editBadgeContainer(badgeContainer: BadgeContainer<any>): void {
    const dialogRef = this.dialog.open(NewBadgeDialogComponent, {
      data: {
        badgeList: this.badgeList,
        listVariable: this.listVariable,
        badgeContainer
      }
    });
    dialogRef.afterClosed().subscribe(bc => {
      if (bc) {
        badgeContainer.name = bc.name;
        badgeContainer.setBadgeList(bc.getBadgeList());
      }
    });
  }
}
