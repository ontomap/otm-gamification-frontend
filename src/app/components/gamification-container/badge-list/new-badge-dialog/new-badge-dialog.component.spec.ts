import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBadgeDialogComponent } from './new-badge-dialog.component';

describe('NewBadgeDialogComponent', () => {
  let component: NewBadgeDialogComponent;
  let fixture: ComponentFixture<NewBadgeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBadgeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBadgeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
