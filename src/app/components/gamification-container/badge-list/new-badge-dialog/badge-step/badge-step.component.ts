import {Component, Input, OnInit} from '@angular/core';
import {Badge, BadgeStep, VariablePrecondition} from '../../../../../model/badge';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {BadgeLogosGalleryComponent} from '../badge-logos-gallery/badge-logos-gallery.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-badge-step',
  templateUrl: './badge-step.component.html',
  styleUrls: ['./badge-step.component.css']
})
export class BadgeStepComponent implements OnInit {
  @Input() badge: BadgeStep;
  @Input() badgeContainerName: string;
  @Input() badgeList: Badge[];
  @Input() variablesFilteredOptions: Observable<string[]>;
  @Input() listVariable: string[];
  @Input() editable: boolean;
  @Input() editMode: boolean;
  @Input() preview: boolean;
  @Input() previousBadgeComponent: BadgeStepComponent;
  // newVariableNameFormControl = new FormControl('', [Validators.required]);
  @Input() badgeFormGroup: FormGroup;
  variablePreconditionFormGroup: FormGroup;
  newPreconditionsBadgeList: any[];
  // newVariableValueFormControl = new FormControl('', [
  //   Validators.required,
  //   Validators.min(1)
  // ]);
  badgeNewLogo: string;

  constructor(private fb: FormBuilder, public galleryDialog: MatDialog) {
  }

  ngOnInit(): void {
    this.badgeNewLogo = this.badge.logo;
    this.variablePreconditionFormGroup = this.fb.group({
      name: ['', [Validators.required]],
      value: ['', [Validators.required, Validators.min(1)]]
    });
    this.variablesFilteredOptions = this.variablePreconditionFormGroup.get('name').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.newPreconditionsBadgeList = [];
  }

  updatePreconditionsNumber(): void {
    const numVariable = this.badge.preconditions.variables.length;
    const numBadges = this.newPreconditionsBadgeList.length;
    this.badgeFormGroup.get('preconditions').setValue(numVariable + numBadges);
  }

  get badgeNewName(): string {
    return this.badgeFormGroup ? this.badgeFormGroup.get('name').value : '';
  }

  openLogoGallery(): void {
    const dialogRef = this.galleryDialog.open(BadgeLogosGalleryComponent, {});

    dialogRef.afterClosed().subscribe(newLogoUrl => {
      this.badgeNewLogo = newLogoUrl ? newLogoUrl : this.badgeNewLogo;
    });
  }

  get preconditionsBadgeList(): Badge [] {
    const p = {};
    if (this.previousBadgeComponent) {
      p[this.previousBadgeComponent.badge.id] = {
        name: this.previousBadgeComponent.badgeNewName,
        badgeContainer: {
          name: this.badgeContainerName
        },
        editable: false,
        removable: false
      };
    }
    if (this.badge.preconditions.badgeList) {
      this.badge.preconditions.badgeList.forEach(prec => {
        p[prec] = this.getBadgeById(prec);
      });
    }
    return Object.values(p);
  }

  addPrecondition(variableName: HTMLInputElement, variableValue: HTMLInputElement): void {
    const variablePrecondition = new VariablePrecondition();
    variablePrecondition.variable = variableName.value;
    variablePrecondition.threshold = variableValue.valueAsNumber;
    const existingVariable = this.badge.preconditions.variables.filter(variable => variable.variable === variablePrecondition.variable)[0];
    if (existingVariable) {
      existingVariable.threshold = variablePrecondition.threshold;
    } else {
      this.badge.preconditions.variables.push(variablePrecondition);
    }
    this.updatePreconditionsNumber();
  }

  addBadgePrecondition(badge: Badge): void {
    if (this.badge.preconditions.badgeList.indexOf(badge.id) < 0 &&
      this.newPreconditionsBadgeList.filter(b => b.id === badge.id).length === 0) {
      // this.badge.preconditions.badgeList.push(badge.id);
      this.newPreconditionsBadgeList.push({...badge});
      this.updatePreconditionsNumber();
    }
  }

  getBadgeById(idBadge: string): Badge {
    return this.badgeList.filter(badge => badge.id === idBadge)[0];
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.listVariable.filter(variable => variable.toLowerCase().indexOf(filterValue) === 0);
  }

  removeVariable(variable: string): void {
    this.badge.preconditions.variables = this.badge.preconditions.variables
      .filter(v => v.variable.toUpperCase() !== variable.toUpperCase());
    this.updatePreconditionsNumber();
  }

  removeBadgePrecondition(idToRemove: string): void {
    this.newPreconditionsBadgeList = this.newPreconditionsBadgeList
      .filter(b => b.id !== idToRemove);
    this.updatePreconditionsNumber();
  }

  // isComponentReady(): boolean {
  //   return this.badgeFormGroup.get('name').valid &&
  //     (this.badge.preconditions.variables.length > 0 ||
  //       this.badge.preconditions.badgeList.length > 0);
  // }

  getBadgeStepToSave(): BadgeStep {
    const badgeToSave = new BadgeStep();
    badgeToSave.name = this.badgeNewName;
    badgeToSave.active = this.badge.active;
    badgeToSave.id = this.badge.id;

    badgeToSave.logo = this.badgeNewLogo;
    badgeToSave.preconditions.variables = this.badge.preconditions.variables;
    badgeToSave.preconditions.badgeList = this.badge.preconditions.badgeList;

    if (!badgeToSave.logo) {
      delete badgeToSave.logo;
    }
    if (this.newPreconditionsBadgeList.length > 0) {
      badgeToSave.preconditions.badgeList = this.newPreconditionsBadgeList.map(p => p.id);
    }
    if (badgeToSave.preconditions.variables && badgeToSave.preconditions.variables.length === 0) {
      delete badgeToSave.preconditions.variables;
    }
    if (badgeToSave.preconditions.badgeList && badgeToSave.preconditions.badgeList.length === 0) {
      delete badgeToSave.preconditions.badgeList;
    }
    delete badgeToSave.removable;
    delete badgeToSave.badgeContainer;
    return badgeToSave;
  }
}
