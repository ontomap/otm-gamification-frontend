import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeStepComponent } from './badge-step.component';

describe('BadgeStepComponent', () => {
  let component: BadgeStepComponent;
  let fixture: ComponentFixture<BadgeStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
