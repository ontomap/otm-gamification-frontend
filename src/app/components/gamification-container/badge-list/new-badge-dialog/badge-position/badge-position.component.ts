import {Component, Input, OnInit} from '@angular/core';
import {Badge, BadgePosition, BadgeStep} from '../../../../../model/badge';
import {FormGroup} from '@angular/forms';
import {BadgeLogosGalleryComponent} from '../badge-logos-gallery/badge-logos-gallery.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-badge-position',
  templateUrl: './badge-position.component.html',
  styleUrls: ['./badge-position.component.css']
})
export class BadgePositionComponent implements OnInit {

  @Input() badge: Badge;
  @Input() badgeFormGroup: FormGroup;

  @Input() editable: boolean;
  @Input() editMode: boolean;
  @Input() preview: boolean;

  badgeNewLogo: string;

  constructor(public galleryDialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  get badgeNewName(): string {
    return this.badgeFormGroup ? this.badgeFormGroup.get('name').value : '';
  }

  get badgeNewPosition(): number {
    return this.badgeFormGroup ? this.badgeFormGroup.get('position').value : null;
  }

  openLogoGallery(): void {
    const dialogRef = this.galleryDialog.open(BadgeLogosGalleryComponent, {});

    dialogRef.afterClosed().subscribe(newLogoUrl => {
      this.badgeNewLogo = newLogoUrl ? newLogoUrl : this.badgeNewLogo;
    });
  }

  positionTextFormatter(): string {
    const pos = this.badgeFormGroup.get('position').value;
    if (!pos) {
      return '';
    }
    let posText = pos + '';
    const last = posText[posText.length - 1];
    switch (last) {
      case '1':
        posText += 'st';
        break;
      case '2':
        posText += 'nd';
        break;
      case '1':
        posText += 'rd';
        break;
      default:
        posText += 'th';
        break;
    }
    return posText;
  }

  getBadgePositionToSave(): BadgePosition {
    const badgeToSave = new BadgePosition();
    badgeToSave.name = this.badgeNewName;
    badgeToSave.active = this.badge.active;
    badgeToSave.id = this.badge.id;
    badgeToSave.position = this.badgeNewPosition;
    badgeToSave.logo = this.badgeNewLogo;

    if (!badgeToSave.logo) {
      delete badgeToSave.logo;
    }

    delete badgeToSave.removable;
    delete badgeToSave.badgeContainer;
    return badgeToSave;
  }
}
