import {Component, ElementRef, NgZone, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {GalleryService} from '../../../../../services/gallery.service';
import {FLImage} from '../../../../../model/flimage';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-badge-logos-gallery',
  templateUrl: './badge-logos-gallery.component.html',
  styleUrls: ['./badge-logos-gallery.component.css']
})
export class BadgeLogosGalleryComponent implements OnInit {

  images: FLImage[];
  imgSelected: FLImage;
  fileSelected: File;
  userId: string;
  uploadInProgress = false;

  constructor(public dialogRef: MatDialogRef<BadgeLogosGalleryComponent>, public galleryService: GalleryService, private elem: ElementRef) {
    this.userId = environment.USER_AUTH.member_id;
  }

  ngOnInit(): void {
    this.galleryService.getImages().subscribe(
      images => this.images = images,
      error => this.images = []
    );
  }

  selectedImage($event, img: FLImage): void {
    this.elem.nativeElement.querySelectorAll('.image-item').forEach(item => item.setAttribute('id', ''));
    $event.currentTarget.setAttribute('id', 'image-selected');
    this.imgSelected = img;
    // $('.test-img-box')[i].attr('id', 'image-selected');
  }

  uploadFile(imageToUpload: File): void {
    this.uploadInProgress = true;
    this.galleryService.uploadImage(imageToUpload).subscribe(
      response => {
        console.log(response);
      },
      error => console.log(error), // TODO
      () => {
        this.uploadInProgress = false;
      }
    );
  }
}
