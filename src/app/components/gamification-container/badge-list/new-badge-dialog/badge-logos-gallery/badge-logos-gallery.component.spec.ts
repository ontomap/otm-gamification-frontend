import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeLogosGalleryComponent } from './badge-logos-gallery.component';

describe('BadgeLogosGalleryComponent', () => {
  let component: BadgeLogosGalleryComponent;
  let fixture: ComponentFixture<BadgeLogosGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeLogosGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeLogosGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
