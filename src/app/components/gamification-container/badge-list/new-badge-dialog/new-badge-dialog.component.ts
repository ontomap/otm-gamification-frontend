import {
  Component,
  Inject,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {
  AcaBadgeContainer,
  BadgeContainer,
  IndividualBadgeContainer,
  RankingBadgeContainer,
  TemporalBadgeContainer
} from '../../../../model/badge-container';
import {Badge, BadgePosition, BadgeStep} from '../../../../model/badge';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AcaService} from '../../../../services/aca.service';
import {ACA} from '../../../../model/aca';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {BadgeStepComponent} from './badge-step/badge-step.component';
import {BadgePositionComponent} from './badge-position/badge-position.component';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {MatSelectChange} from '@angular/material/select';
import {Rule} from '../../../../model/rule';
import {BadgeService} from '../../../../services/badge.service';

export interface DialogData {
  badgeContainer: BadgeContainer<Badge>;
  badgeList: Badge[];
  listVariable: string[];
}

@Component({
  selector: 'app-new-badge-dialog',
  templateUrl: './new-badge-dialog.component.html',
  styleUrls: ['./new-badge-dialog.component.css']
})
export class NewBadgeDialogComponent implements OnInit {
  badgeContainerType = [
    {
      key: 'individual',
      label: 'Individual'
    },
    {
      key: 'aca',
      label: 'ACA'
    },
    {
      key: 'ranking',
      label: 'Ranking'
    },
  ];
  badgeContainerTimeframe = [
    {
      key: 'WEEK',
      label: 'Weekly'
    },
    {
      key: 'MONTH',
      label: 'Monthly'
    }
  ];
  acaList: ACA[];
  listVariable: string[];

  badgeContainer: BadgeContainer<Badge>;
  badgeContainerToSave: BadgeContainer<Badge>;
  newBadgesList: Badge[];
  badgeList: Badge[];
  editMode = true;

  badgeContainerFormGroup: FormGroup;
  badgeListFormGroup: FormGroup;
  @ViewChildren(BadgeStepComponent) private badgeStepComponents: QueryList<BadgeStepComponent>;
  @ViewChildren(BadgePositionComponent) private badgePositionComponents: QueryList<BadgePositionComponent>;

  containerReady = () => false;
  getBadgeStepComponent = (index) => undefined;

  constructor(public dialogRef: MatDialogRef<NewBadgeDialogComponent>,
              // tslint:disable-next-line:variable-name
              @Inject(MAT_DIALOG_DATA) public data: DialogData, public acaService: AcaService, private badgeService: BadgeService,
              private fb: FormBuilder) {
    if (data.badgeContainer) {
      // this.badgeContainerTemp =  JSON.parse(JSON.stringify(data.badgeContainer));
      this.badgeContainer = data.badgeContainer;
    } else {
      this.badgeContainer = new IndividualBadgeContainer();
      this.editMode = false;
    }
    this.badgeList = data.badgeList;
    this.newBadgesList = [];
    acaService.getAcas().subscribe(acas => {
      this.acaList = acas;
    });
    this.listVariable = data.listVariable;
  }

  ngOnInit(): void {
    const timeframe = this.badgeContainer instanceof TemporalBadgeContainer ? this.badgeContainer.getTimeframe() : null;
    const variable = this.badgeContainer instanceof RankingBadgeContainer ? this.badgeContainer.getVariable() : null;

    this.badgeContainerFormGroup = this.fb.group({
      name: new FormControl(this.badgeContainer.name, [Validators.minLength(3), Validators.required]),
      type: new FormControl(this.badgeContainer.getType(), [Validators.required]),
      aca: new FormControl(this.badgeContainer.aca),
      timeframe: new FormControl(timeframe),
      variable: new FormControl(variable),
    });
    if (this.editMode) {
      this.badgeContainerFormGroup.get('type').disable();
      this.badgeContainerFormGroup.get('aca').disable();
      this.badgeContainerFormGroup.get('timeframe').disable();
      this.badgeContainerFormGroup.get('variable').disable();
    }
    this.badgeContainerFormGroup.get('type').valueChanges.subscribe(type => {
      if (type !== 'individual') {
        this.badgeContainerFormGroup.get('timeframe').setValidators([Validators.required]);
      } else {
        this.badgeContainerFormGroup.get('timeframe').clearValidators();
        this.badgeContainerFormGroup.get('timeframe').updateValueAndValidity();
      }
      if (type === 'ranking') {
        this.badgeContainerFormGroup.get('variable').setValidators([Validators.required]);
      } else {
        this.badgeContainerFormGroup.get('variable').clearValidators();
        this.badgeContainerFormGroup.get('variable').updateValueAndValidity();
      }
    });
    this.badgeListFormGroup = this.fb.group({});
    if (this.badgeContainer instanceof RankingBadgeContainer) {
      this.badgeListFormGroup.setValidators(UniquePositionValidator);
    }
    this.badgeContainer.getBadgeList().forEach(b => this.badgeListFormGroup.addControl(b.id, this.buildBadgeFormControl(b)));
    this.buildBadgeContainerToSave();
    // setTimeout(() => this.containerReady = () => this.isBadgeContainerReady(), 0);
    setTimeout(() => this.getBadgeStepComponent = (index) => this.getBadgeStepComponentInner(index), 0);
  }


  getBadgeStepComponentInner(index: number): BadgeStepComponent {
    return this.badgeStepComponents ? this.badgeStepComponents.toArray()[index] : undefined;
  }

  get badgeContainerNewName(): string {
    return this.badgeContainerFormGroup.get('name').value;
  }

  get badgeContainerNewType(): string {
    return this.badgeContainerFormGroup.get('type').value;
  }

  badgeContainerTypeChange($event: MatSelectChange): void {
    this.newBadgesList = [];
    this.badgeContainer.getBadgeList().forEach(b => this.badgeListFormGroup.addControl(b.id, this.buildBadgeFormControl(b)));
    if ($event.value === 'ranking') {
      this.badgeListFormGroup.setValidators(UniquePositionValidator);
    } else {
      this.badgeListFormGroup.clearValidators();
      this.badgeListFormGroup.updateValueAndValidity();
    }
  }

  get badgeContainerNewTimeframe(): string {
    return this.badgeContainerFormGroup.get('timeframe').value;
  }

  get badgeContainerNewAcas(): string {
    return this.badgeContainerFormGroup.get('aca').value;
  }

  get badgeContainerNewVariable(): string {
    return this.badgeContainerFormGroup.get('variable').value;
  }

  get allBadgesList(): Badge[] {
    return this.badgeList.concat(this.newBadgesList);
  }

  clickNewBadge(): void {
    let badge;
    if (this.badgeContainerNewType === 'ranking') {
      badge = new BadgePosition();
    } else {
      badge = new BadgeStep();
    }
    badge.badgeContainer = this.badgeContainer;
    this.badgeListFormGroup.addControl(badge.id, this.buildBadgeFormControl(badge));
    this.newBadgesList.push(badge);
  }

  buildBadgeFormControl(badge: Badge): FormGroup {
    if (badge instanceof BadgeStep) {
      return this.fb.group({
        name: new FormControl(badge.name, [Validators.required, Validators.minLength(3)]),
        preconditions: new FormControl(1, [Validators.required, Validators.min(1)])
      });
    } else {
      return this.fb.group({
        name: new FormControl(badge.name, [Validators.required, Validators.minLength(3)]),
        position: new FormControl((badge as BadgePosition).position, [Validators.required])
      });
    }
  }

  buildBadgeContainerToSave(): void {
    // this.badgeContainerToSave = new BadgeContainer();
    switch (this.badgeContainerNewType) {
      case 'individual':
        this.badgeContainerToSave = new IndividualBadgeContainer();
        break;
      case 'aca':
        this.badgeContainerToSave = new AcaBadgeContainer();
        (this.badgeContainerToSave as AcaBadgeContainer).timeframe = this.badgeContainerNewTimeframe;
        break;
      case 'ranking':
        this.badgeContainerToSave = new RankingBadgeContainer();
        (this.badgeContainerToSave as RankingBadgeContainer).timeframe = this.badgeContainerNewTimeframe;
        (this.badgeContainerToSave as RankingBadgeContainer).variable = this.badgeContainerNewVariable;
        break;
    }
    if (this.editMode){
      this.badgeContainerToSave.id = this.badgeContainer.id;
    }
    this.badgeContainerToSave.name = this.badgeContainerNewName;
    this.badgeContainerToSave.aca = this.badgeContainerNewAcas;
    if (this.badgeContainerToSave.aca === null) {
      delete this.badgeContainerToSave.aca;
    }
    if (this.badgeStepComponents) {
      this.badgeStepComponents
        .filter(c => !c.preview)
        .forEach(component => {
          const badge = component.getBadgeStepToSave();
          if (badge.preconditions.badgeList && badge.preconditions.badgeList.length === 0) {
            delete badge.preconditions.badgeList;
          }
          this.badgeContainerToSave.getBadgeList().push(badge);
        });
    }
    if (this.badgePositionComponents){
      this.badgePositionComponents
        .filter(c => !c.preview)
        .forEach(component => {
          const badge = component.getBadgePositionToSave();
          this.badgeContainerToSave.getBadgeList().push(badge);
        });
    }
  }

  stepChange($event: StepperSelectionEvent): void {
    if ($event.selectedIndex === 2) {
      this.buildBadgeContainerToSave();
    }
  }

  saveBadgeContainer(): void {
    console.log(JSON.stringify(this.badgeContainerToSave));
    if (this.editMode){
      this.badgeService.updateBadgeContainer(this.badgeContainerToSave).subscribe(res => {
        const badgeBadgeContainer = BadgeContainer.fromJson(res[0]);
        this.dialogRef.close(badgeBadgeContainer);
      });
    }
    else {
      this.badgeService.saveBadgeContainer(this.badgeContainerToSave).subscribe(res => {
        const badgeBadgeContainer = BadgeContainer.fromJson(res[0]);
        this.dialogRef.close(badgeBadgeContainer);
      });
    }
  }
}



export const UniquePositionValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const positions = Object.values(control.value).map(({name, position}) => position);
  const hasDuplicates = (new Set(positions)).size !== positions.length;

  return hasDuplicates ? { duplicates: true } : null;
};


