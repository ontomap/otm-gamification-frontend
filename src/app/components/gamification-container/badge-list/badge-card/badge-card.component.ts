import {Component, Input, OnInit} from '@angular/core';
import {Badge, BadgePosition, BadgeStep} from '../../../../model/badge';
import {OrgData} from 'angular-org-chart/src/app/modules/org-chart/orgData';
import {BadgePreconditionsDialogComponent} from '../badge-preconditions-dialog/badge-preconditions-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {BadgeContainer} from '../../../../model/badge-container';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {Rule} from '../../../../model/rule';
import {ConfirmDialogComponent} from '../../../confirm-dialog/confirm-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {BadgeService} from '../../../../services/badge.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-badge-card',
  templateUrl: './badge-card.component.html',
  styleUrls: ['./badge-card.component.scss']
})
export class BadgeCardComponent implements OnInit {
  @Input() badge: Badge;
  @Input() badgeContainer: BadgeContainer<Badge>;
  @Input() badgeList: Badge[];

  constructor(public dialog: MatDialog, private translate: TranslateService,
              private badgeService: BadgeService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BadgePreconditionsDialogComponent, {
      data: this.createTree(null, this.badge)
    });
  }

  createTree(root: OrgData, badge: Badge): OrgData {
    const tree = {
      name: badge.badgeContainer.name + ' - ' + badge.name,
      type: 'Badge',
      children: []
    };
    if (badge instanceof BadgeStep) {
      if (badge.preconditions && badge.preconditions.variables) {
        badge.preconditions.variables.forEach(variable => {
          const varTree = {
            name: variable.threshold + ' ' + variable.variable,
            type: 'Variable',
            children: []
          };
          tree.children.push(varTree);
        });
      }
      if (badge.preconditions && badge.preconditions.badgeList) {
        badge.preconditions.badgeList.forEach(bId => {
          const b = this.badgeList.filter(bInner => {
            return bInner.id === bId;
          })[0];
          tree.children.push(this.createTree(tree, b));
        });
      }
    }
    return tree;
  }


  changeActiveBadge(badge: Badge): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: badge.active ? this.translate.instant('badges.activateConfirm') : this.translate.instant('badges.disableConfirm')
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.badgeService.enableBadge(badge, badge.active).subscribe(
          () => {
            this.snackBar.open(
              badge.active ? this.translate.instant('badges.activated') : this.translate.instant('badges.disabled'),
              null,
              {
                duration: 5000
              });
          },
          error => {
            console.log(error);
            badge.active = !badge.active;
            switch (error.status) {
              case 403:
                this.snackBar.open('Error: ' + error.error.message, null, {
                  duration: 5000
                });
                break;
              default:
                break;
            }
          });
      } else {
        badge.active = !badge.active;
      }
    });
  }

  canBeEnabled(): boolean {
    const index = this.badgeContainer.getBadgeList().indexOf(this.badge);
    const nextBadge = index < this.badgeContainer.getBadgeList().length ? this.badgeContainer.getBadgeList()[index + 1] : null;
    const prevBadge = index > 0 ? this.badgeContainer.getBadgeList()[index - 1] : null;
    if (!nextBadge && !prevBadge) {
      return true;
    }
    if (!nextBadge) {
      return prevBadge.active;
    }
    if (!prevBadge) {
      return !nextBadge.active;
    }
    return prevBadge.active && !nextBadge.active;
  }
}
