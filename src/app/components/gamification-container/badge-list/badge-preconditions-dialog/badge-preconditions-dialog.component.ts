import {Component, Inject, OnInit} from '@angular/core';
import {OrgData} from 'angular-org-chart/src/app/modules/org-chart/orgData';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-badge-preconditions-dialog',
  templateUrl: './badge-preconditions-dialog.component.html',
  styleUrls: ['./badge-preconditions-dialog.component.css']
})
export class BadgePreconditionsDialogComponent implements OnInit {
  badgeTree: OrgData;

  constructor(
    public dialogRef: MatDialogRef<BadgePreconditionsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OrgData) {
    this.badgeTree = data;
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
