import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgePreconditionsDialogComponent } from './badge-preconditions-dialog.component';

describe('BadgePreconditionsDialogComponent', () => {
  let component: BadgePreconditionsDialogComponent;
  let fixture: ComponentFixture<BadgePreconditionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgePreconditionsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgePreconditionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
