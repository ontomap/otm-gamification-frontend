import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatSelectChange} from '@angular/material/select';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {BadgeLog, GamificationLog, RuleLog} from '../../../model/gamification-log';
import {TranslateService} from '@ngx-translate/core';
import {GamificationLogService} from '../../../services/gamification-log.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Rule} from '../../../model/rule';
import {ACA} from '../../../model/aca';
import {AcaService} from '../../../services/aca.service';
import {MatDialog} from '@angular/material/dialog';
import {RuleDialogComponent} from './rule-dialog/rule-dialog.component';
import {MatDatepickerInputEvent, MatDateRangePicker} from '@angular/material/datepicker';
import {Moment} from 'moment/moment';
import {DateAdapter} from '@angular/material/core';
import {Badge} from '../../../model/badge';
import {merge, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-gamification-log-list',
  templateUrl: './gamification-log-list.component.html',
  styleUrls: ['./gamification-log-list.component.scss']
})
export class GamificationLogListComponent implements OnInit, AfterViewInit {
  dataSource: MatTableDataSource<GamificationLog>;
  displayedColumns: string[] = ['actor', 'date', 'logType', 'aca', 'group', 'rule', 'badge', 'activity'];
  acaMap: { [key: string]: ACA; } = {};
  acaList: ACA[];
  acaListLogs: { [key: string]: ACA; } = {};

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) logTable: MatTable<GamificationLog>;
  resultsLength = 0;

  logType: string = null;
  fromDate: Date;
  untilDate: Date;
  maxDate: Date;
  selectedAca: string = null;

  isLoadingResults = true;

  constructor(private translate: TranslateService, private logsService: GamificationLogService,
              // tslint:disable-next-line:variable-name
              public acaService: AcaService, public dialog: MatDialog, private _adapter: DateAdapter<any>) {
    this.maxDate = new Date();
    _adapter.setLocale('fr');
  }

  ngOnInit(): void {
    this.acaService.getAcas().subscribe(acaList => {
      this.acaList = acaList;
      acaList.forEach(aca => {
        this.acaMap[aca.url] = aca;
      });
    });
  }


  ngAfterViewInit(): void {
    this.loadLogs();
  }

  loadLogs(): void {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.logsService.getLogs(this.selectedAca, this.areRulesSelected, this.areBadgeSelected, this.fromDate,
            this.untilDate, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          console.log(data);
          this.isLoadingResults = false;
          this.resultsLength = data.totalCount;
          return data.results;
        }),
        catchError((err) => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      )
      .subscribe(data => {
        console.log(data);
        this.dataSource = new MatTableDataSource(data);
        data.forEach(log => {
          if (log.aca) {
            if (log.aca in this.acaMap) {
              this.acaListLogs[log.aca] = this.acaMap[log.aca];
            } else {
              const newACA = new ACA();
              newACA.name = log.aca;
              newACA.url = log.aca;
              this.acaListLogs[log.aca] = newACA;
            }
          }
        });
      });
  }

  get areBadgeSelected(): boolean {
    return this.logType === null || this.logType === undefined || this.logType === 'badges';
  }

  get areRulesSelected(): boolean {
    return this.logType === null || this.logType === undefined || this.logType === 'rules';
  }


  openRuleDialog(rule: Rule): void {
    const dialogRef = this.dialog.open(RuleDialogComponent, {
      width: '500px',
      data: {
        rule,
        acaList: this.acaList
      }
    });
  }

  downloadAsJson(): void {
    this.logsService.getLogs(this.selectedAca, this.areRulesSelected, this.areBadgeSelected, this.fromDate,
      this.untilDate, -1, -1).subscribe(
      data => {
        const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data));
        const downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute('href', dataStr);
        downloadAnchorNode.setAttribute('download', 'gamification_logs.json');
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
      }
    );
  }

  intervalChanged(dateType: string, $event: MatDatepickerInputEvent<Moment>): void {
    if (dateType === 'from') {
      if ($event.value) {
        this.fromDate = $event.value.toDate();
      } else {
        this.fromDate = null;
      }
    } else {
      if ($event.value) {
        this.untilDate = $event.value.toDate();
      } else {
        this.untilDate = null;
      }
    }
    if (dateType === 'until') {
      this.loadLogs();
    }
  }

  getAcaName(url: string): string {
    if (url in this.acaMap) {
      return this.acaMap[url].name;
    }
    return 'aca_not_found';
  }

  isLogRule(row: GamificationLog): boolean {
    return row instanceof RuleLog;
  }

  getActivityURL(properties): string {
    return properties.hasWebView ? properties.hasWebView : properties.external_url;
  }
}
