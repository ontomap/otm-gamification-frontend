import {Component, Inject, OnInit} from '@angular/core';
import {ACA} from '../../../../model/aca';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Rule} from '../../../../model/rule';

@Component({
  selector: 'app-rule-dialog',
  templateUrl: './rule-dialog.component.html',
  styleUrls: ['./rule-dialog.component.css']
})
export class RuleDialogComponent implements OnInit {
  rule: Rule;
  acaList: ACA[];

  constructor(
    public dialogRef: MatDialogRef<RuleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.rule = data.rule;
    this.acaList = data.acaList;
  }

  ngOnInit(): void {
  }

}
