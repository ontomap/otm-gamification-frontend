import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamificationLogListComponent } from './gamification-log-list.component';

describe('GamificationLogListComponent', () => {
  let component: GamificationLogListComponent;
  let fixture: ComponentFixture<GamificationLogListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamificationLogListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamificationLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
