import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {ObjectProperty, Rule} from '../../../model/rule';
import {RuleService} from '../../../services/rule.service';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../confirm-dialog/confirm-dialog.component';
import {MatSelectChange} from '@angular/material/select';
import {SelectionModel} from '@angular/cdk/collections';
import {NewRuleDialogComponent} from './new-rule-dialog/new-rule-dialog.component';
import {getRoleToString} from '../../../model/rule';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {retry} from 'rxjs/operators';

export interface DialogData {
  rules: Rule[];
}

@Component({
  selector: 'app-rule-list',
  templateUrl: './rule-list.component.html',
  styleUrls: ['./rule-list.component.scss']
})
export class RuleListComponent implements OnInit {
  displayedColumns: string[] = ['activity_type', 'active',
    'rewards', 'activityObjectProperties', 'referenceProperties',
    'acas', 'repetitions', 'validity'];
  dataSource: MatTableDataSource<Rule>;
  selection = new SelectionModel<Rule>(true, []);
  listActivityType: string[];
  listVariable: string[];
  listConcept: string[];
  activityTypeSelected: string;
  variableSelected: string;
  conceptSelect: string;
  ruleList: Rule[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) ruleTable: MatTable<Rule>;

  myGetRoleToString = getRoleToString;

  constructor(private translate: TranslateService, private ruleService: RuleService,
              public dialog: MatDialog, private snackBar: MatSnackBar) {
    ruleService.getRules().subscribe(data => {
      this.ruleList = data;
      this.dataSource = new MatTableDataSource(this.ruleList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this._updateFilters();
    });
  }


  ngOnInit(): void {
  }

  _updateFilters(): void {
    const activityTypeDict = {};
    const variableDict = {};
    const conceptDict = {};

    this.dataSource.data.forEach(rule => {
      activityTypeDict[rule.activity_type] = true;
      Object.values(rule.rewards).forEach(variables => {
        variables.forEach(variable => {
          variableDict[variable.name] = true;
        });
      });
      if (rule.activityObjectProperties && rule.activityObjectProperties.length > 0) {
        conceptDict[rule.activityObjectProperties.filter(aop => aop.propertyName === 'hasType')[0].propertyValue] = true;
      }
    });
    this.listActivityType = Object.keys(activityTypeDict);
    this.listVariable = Object.keys(variableDict);
    this.listConcept = Object.keys(conceptDict);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(rule => this.selection.select(rule));
  }

  openNewBadgeDialog(): void {
    const dialogRef = this.dialog.open(NewRuleDialogComponent, {
      data: {
        listVariable: this.listVariable,
      }
    });
    dialogRef.afterClosed().subscribe(newRule => {
      if (newRule) {
        this.ruleList.push(newRule);
        this.dataSource = new MatTableDataSource(this.ruleList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this._updateFilters();
      }
    });
  }

  selectedFilterChange(type: string, event: MatSelectChange): void {
    if (type === 'activityType') {
      this.activityTypeSelected = event.value;
    } else if (type === 'variable') {
      this.variableSelected = event.value;
    } else if (type === 'concept') {
      this.conceptSelect = event.value;
    }
    this.dataSource.filterPredicate = (data: Rule, [ats, vs, cs]) => {
      let result;
      result = !ats || data.activity_type === ats;
      result = result && (!vs || data.getVariableNames().indexOf(vs) > -1);
      if (cs) {
        if (data.activityObjectProperties) {
          const hasTypeProp = data.activityObjectProperties.filter(aop => aop.propertyName === 'hasType')[0];
          result = result && (hasTypeProp.propertyValue === cs);
        } else if (data.referenceObjectProperties) {
          const hasTypeProp = data.referenceObjectProperties.filter(aop => aop.propertyName === 'hasType')[0];
          result = result && (hasTypeProp.propertyValue === cs);
        } else {
          result = false;
        }
      }
      return result;
    };
    // @ts-ignore
    this.dataSource.filter = [this.activityTypeSelected, this.variableSelected, this.conceptSelect];
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ruleChangeActive(rule: Rule): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: rule.active ? this.translate.instant('rules.activateConfirm') : this.translate.instant('rules.disableConfirm')
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ruleService.changeActive(rule.id, rule.active).subscribe(
          () => {
            this.snackBar.open(rule.active ? this.translate.instant('rules.activated') : this.translate.instant('rules.disabled'), null, {
              duration: 5000
            });
          },
          error => {
            console.log(error);
            rule.active = !rule.active;
            switch (error.status) {
              case 403:
                this.snackBar.open('Error: ' + error.error.message, null, {
                  duration: 5000
                });
                break;
              default:
                break;
            }
          });
      } else {
        rule.active = !rule.active;
      }
    });
  }

  formatDate(validFrom: number): string {
    if (validFrom){
      return new Date(validFrom).toLocaleString();
    }
    return null;
  }
}

