import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulePreviewComponent } from './rule-preview.component';

describe('RulePreviewComponent', () => {
  let component: RulePreviewComponent;
  let fixture: ComponentFixture<RulePreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RulePreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RulePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
