import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ACA} from '../../../../../model/aca';
import {ObjectProperty, Rule} from '../../../../../model/rule';
import {MatTable} from '@angular/material/table';

@Component({
  selector: 'app-rule-preview',
  templateUrl: './rule-preview.component.html',
  styleUrls: ['./rule-preview.component.css']
})
export class RulePreviewComponent implements OnInit {
  @Input() acaList: ACA[];
  @Input() rule: Rule;

  @ViewChild('AOPTablePreview') AOPTablePreview: MatTable<ObjectProperty>;
  @ViewChild('ROPTablePreview') ROPTablePreview: MatTable<ObjectProperty>;
  @ViewChild('ActorRewardsTablePreview') ActorRewardsTablePreview: MatTable<ObjectProperty>;
  @ViewChild('RefOwnRewardsTablePreview') RefOwnRewardsTablePreview: MatTable<ObjectProperty>;

  displayedColumnsPreview: string[] = ['name', 'value'];
  displayedColumnsAssignmentPreview: string[] = ['variable', 'assignment'];

  constructor() {
    console.log(this.rule);
  }

  ngOnInit(): void {
    console.log(this.rule);
  }

  get acas(): ACA[] {
    if (this.rule.acas){
      return this.rule.acas.map(aca => this.acaList.filter(aca2 => aca2.url === aca)[0]);
    }
    return null;
  }

  get conceptSelected(): string {
    return this.rule.activityObjectProperties ?
      this.rule.activityObjectProperties.filter(aop => aop.propertyName === 'hasType')[0].propertyValue :
      null;
  }

  // @ts-ignore
  set conceptSelected(newValue: string): void {  }

  get activityObjectProperties(): ObjectProperty[] {
    return this.rule.activityObjectProperties ?
      this.rule.activityObjectProperties.filter(aop => aop.propertyName !== 'hasType') :
      [];
  }


  get referenceSelected(): string {
    return this.rule.referenceObjectProperties ?
      this.rule.referenceObjectProperties.filter(aop => aop.propertyName === 'hasType')[0].propertyValue :
      null;
  }

  get referenceObjectProperties(): ObjectProperty[] {
    return this.rule.referenceObjectProperties ?
      this.rule.referenceObjectProperties.filter(aop => aop.propertyName !== 'hasType') :
      [];
  }

  update(): void {
    if (this.AOPTablePreview) {
      this.AOPTablePreview.renderRows();
    }
    if (this.ROPTablePreview) {
      this.ROPTablePreview.renderRows();
    }

    if (this.ActorRewardsTablePreview) {
      this.ActorRewardsTablePreview.renderRows();
    }
    if (this.RefOwnRewardsTablePreview) {
      this.RefOwnRewardsTablePreview.renderRows();
    }
  }


}
