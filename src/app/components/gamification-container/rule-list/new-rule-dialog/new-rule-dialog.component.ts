import {Component, Inject, Input, OnInit, Query, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {OTMConcept, OTMProperty} from '../../../../model/otmconcept';
import {ACA} from '../../../../model/aca';
import {OTMService} from '../../../../services/otmservice.service';
import {AcaService} from '../../../../services/aca.service';
import {ObjectProperty, Role, Rule, Variable} from '../../../../model/rule';
import {PropertySelectionDialogComponent} from './property-selection-dialog/property-selection-dialog.component';
import {getRoleToString} from '../../../../model/rule';
import {MatTable} from '@angular/material/table';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {RuleService} from '../../../../services/rule.service';
import {RewardDefineDialogComponent} from './reward-define-dialog/reward-define-dialog.component';
import {RulePreviewComponent} from './rule-preview/rule-preview.component';


export interface NewRuleDialogData {
  listVariable: string[];
}


@Component({
  selector: 'app-new-rule-dialog',
  templateUrl: './new-rule-dialog.component.html',
  styleUrls: ['./new-rule-dialog.component.css']
})
export class NewRuleDialogComponent implements OnInit {
  templates: RuleTemplate[];
  templatesFormGroup: FormGroup;
  ruleCreationFormGroup: FormGroup;
  templatesOptions: Observable<any[]>;
  activityTypes: string[];
  concepts: string[];
  acaList: ACA[];
  conceptSelected: OTMConcept;
  referenceSelected: OTMConcept;
  conceptProperties: OTMProperty[];
  referenceProperties: OTMProperty[];
  activityObjectProperties: ObjectProperty[];
  referenceObjectProperties: ObjectProperty[];
  actorRewards: Variable[];
  refOwnRewards: Variable[];
  listVariable: string[];
  rule: Rule;

  variablesFilteredOptions = {};
  @ViewChild('AOPTable') AOPTable: MatTable<ObjectProperty>;
  @ViewChild('ROPTable') ROPTable: MatTable<ObjectProperty>;
  @ViewChild('ActorRewardsTable') ActorRewardsTable: MatTable<ObjectProperty>;
  @ViewChild('RefOwnRewardsTable') RefOwnRewardsTable: MatTable<ObjectProperty>;
  @ViewChild(RulePreviewComponent) rulePreview: RulePreviewComponent;

  myGetRoleToString = getRoleToString;
  propsDisplayedColumns: string[] = ['delete', 'name', 'value'];
  propsDisplayedColumnsAssignment: string[] = ['delete', 'variable', 'assignment'];

  constructor(@Inject(MAT_DIALOG_DATA) public data: NewRuleDialogData, public dialog: MatDialog,
              public dialogRef: MatDialogRef<NewRuleDialogComponent>, private fb: FormBuilder,
              private otmService: OTMService, public acaService: AcaService, private ruleService: RuleService) {
    otmService.getConceptList().subscribe(r => this.concepts = r);
    otmService.getActivityTypes().subscribe(r => this.activityTypes = r);
    acaService.getAcas().subscribe(acas => this.acaList = acas);
    this.listVariable = data.listVariable;
    this.rule = new Rule();
    this.conceptProperties = [];
    this.referenceProperties = [];
    this.activityObjectProperties = [];
    this.referenceObjectProperties = [];
    this.actorRewards = [];
    this.refOwnRewards = [];
    this.templates = templateBuilder();
  }

  ngOnInit(): void {
    this.templatesFormGroup = this.fb.group({
      templateSearch: new FormControl(null, []),
      templateSelection: new FormControl(null, []),
    });
    this.ruleCreationFormGroup = this.fb.group({
      acas: new FormControl(null, []),
      activityType: new FormControl(null, [Validators.required]),
      activityObjectType: new FormControl(null, []),
      referenceObjectType: new FormControl(null, []),
      rewards: this.fb.group({
        actor: new FormControl(this.actorRewards, []),
        refOwn: new FormControl(this.refOwnRewards, [])
      }, {validators: RewardsValidator}),
      temporal: this.fb.group({
        validFrom: new FormControl(null, []),
        validTo: new FormControl(null, [BeforeNowValidator]),
      }, {validators: TemporalValidator}),
      repetitions: this.fb.group({
        day: new FormControl(null, [Validators.min(1)]),
        ever: new FormControl(null, [Validators.min(1)])
      }, {validators: RepetitionsValidator})
    });

    this.templatesOptions = this.templatesFormGroup.get('templateSearch').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.templatesFormGroup.get('templateSelection').valueChanges.subscribe(template => this.changeTemplate(template));
    this.ruleCreationFormGroup.get('activityObjectType').valueChanges.subscribe(type => this.activityObjectTypeChange(type));
    this.ruleCreationFormGroup.get('referenceObjectType').valueChanges.subscribe(type => this.referenceTypeChange(type));
  }

  saveRule(): void {
    this.ruleService.saveRule(this.rule).subscribe(res => {
      const rule = Rule.fromJson(res[0]);
      this.dialogRef.close(rule);
    });
  }

  changeTemplate(template: RuleTemplate): void {
    this.ruleCreationFormGroup.reset();
    if (!template) {
      return;
    }
    this.ruleCreationFormGroup.get('activityType').setValue(template.activity_type);
    this.activityObjectProperties = [];
    this.referenceObjectProperties = [];
    if (template.activityObjectProperties) {
      template.activityObjectProperties.forEach(op => {
        switch (op.propertyName) {
          case 'hasType':
            this.ruleCreationFormGroup
              .get('activityObjectType').setValue(op.propertyValue);
            break;
          default:
            this.addAOP(op);
            break;
        }
      });
    }
    if (template.referenceObjectProperties) {
      template.referenceObjectProperties.forEach(op => {
        switch (op.propertyName) {
          case 'hasType':
            this.ruleCreationFormGroup
              .get('referenceObjectType').setValue(op.propertyValue);
            break;
          default:
            this.addROP(op);
            break;
        }
      });
    }
    this.actorRewards = [];
    this.ruleCreationFormGroup.get('rewards').get('actor').setValue(this.actorRewards);
    this.ActorRewardsTable.renderRows();
    this.refOwnRewards = [];
    this.ruleCreationFormGroup.get('rewards').get('refOwn').setValue(this.refOwnRewards);
    this.RefOwnRewardsTable.renderRows();
  }

  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.templates.filter(p => p.activity.toLowerCase().indexOf(filterValue) > -1);
  }

  activityObjectTypeChange(type: string): void {
    if (type) {
      this.otmService.getConceptProperties(type).subscribe(c => {
        this.conceptSelected = c;
      });
    } else {
      this.conceptSelected = null;
    }
    this.activityObjectProperties = [];
    this.AOPTable.renderRows();
  }

  referenceTypeChange(type: string): void {
    if (type) {
      this.otmService.getConceptProperties(type).subscribe(c => {
        this.referenceSelected = c;
      });
    } else {
      this.referenceSelected = null;
    }
    this.referenceObjectProperties = [];
    this.ROPTable.renderRows();
  }

  openConceptNewPropertyDialog(): void {
    const dialogRef = this.dialog.open(PropertySelectionDialogComponent, {
      data: {
        concept: this.conceptSelected
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addAOP(result);
      }
    });
  }

  openReferenceNewPropertyDialog(): void {
    const dialogRef = this.dialog.open(PropertySelectionDialogComponent, {
      data: {
        concept: this.referenceSelected
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addROP(result);
      }
    });
  }

  addAOP(prop: ObjectProperty): void {
    this.activityObjectProperties.push(prop);
    this.AOPTable.renderRows();
  }

  addROP(prop: ObjectProperty): void {
    this.referenceObjectProperties.push(prop);
    this.ROPTable.renderRows();
  }

  deleteAOP(prop: ObjectProperty): void {
    this.activityObjectProperties = this.activityObjectProperties.filter(p => p !== prop);
    this.AOPTable.renderRows();
  }

  deleteROP(prop: ObjectProperty): void {
    this.referenceObjectProperties = this.referenceObjectProperties.filter(p => p !== prop);
    this.ROPTable.renderRows();
  }

  openNewActorRewardDialog(): void {
    const dialogRef = this.dialog.open(RewardDefineDialogComponent, {
      data: {
        listVariable: this.listVariable
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addAReward(result);
      }
    });
  }

  openNewRefOwnRewardDialog(): void {
    const dialogRef = this.dialog.open(RewardDefineDialogComponent, {
      data: {
        listVariable: this.listVariable
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addROReward(result);
      }
    });
  }

  addAReward(variable: Variable): void {
    this.actorRewards.push(variable);
    this.ActorRewardsTable.renderRows();
    this.ruleCreationFormGroup.get('rewards').get('actor').updateValueAndValidity();
  }

  deleteAReward(variable: Variable): void {
    const index = this.actorRewards.indexOf(variable);
    if (index > -1) {
      this.actorRewards.splice(index, 1);
    }
    this.ActorRewardsTable.renderRows();
    this.ruleCreationFormGroup.get('rewards').get('actor').updateValueAndValidity();
  }

  addROReward(variable: Variable): void {
    this.refOwnRewards.push(variable);
    this.RefOwnRewardsTable.renderRows();
    this.ruleCreationFormGroup.get('rewards').get('refOwn').updateValueAndValidity();
  }

  deleteROReward(variable: Variable): void {
    const index = this.refOwnRewards.indexOf(variable);
    if (index > -1) {
      this.refOwnRewards.splice(index, 1);
    }
    this.RefOwnRewardsTable.renderRows();
    this.ruleCreationFormGroup.get('rewards').get('refOwn').updateValueAndValidity();
  }

  changeStep($event: StepperSelectionEvent): void {
    if ($event.selectedIndex === 1) {
      this.rulePreview.update();

      this.rule = new Rule();
      delete this.rule.id;
      this.rule.activity_type = this.ruleCreationFormGroup.get('activityType').value;
      if (this.ruleCreationFormGroup.get('acas').value && this.ruleCreationFormGroup.get('acas').value.length > 0) {
        this.rule.acas = (this.ruleCreationFormGroup.get('acas').value as ACA[]).map(aca => aca.url);
      }
      if (this.conceptSelected) {
        this.rule.activityObjectProperties = this.activityObjectProperties.concat(new ObjectProperty('hasType', this.conceptSelected.id));
      }
      if (this.referenceSelected) {
        this.rule.referenceObjectProperties = this.referenceObjectProperties
          .concat(new ObjectProperty('hasType', this.referenceSelected.id));
      }
      this.rule.rewards = {};
      if (this.actorRewards.length > 0) {
        this.rule.rewards[Role.ACTOR] = this.actorRewards;
      }
      if (this.refOwnRewards.length > 0) {
        this.rule.rewards[Role.REFERENCE_OWNER] = this.refOwnRewards;
      }

      if (this.ruleCreationFormGroup.get('temporal').value.validFrom) {
        this.rule.validFrom = new Date(this.ruleCreationFormGroup.get('temporal').value.validFrom).getTime();
      }
      if (this.ruleCreationFormGroup.get('temporal').value.validTo) {
        this.rule.validTo = new Date(this.ruleCreationFormGroup.get('temporal').value.validTo).getTime();
      }
      if (this.ruleCreationFormGroup.get('repetitions').value.day) {
        this.rule.maxEvDay = this.ruleCreationFormGroup.get('repetitions').value.day;
      }
      if (this.ruleCreationFormGroup.get('repetitions').value.ever) {
        this.rule.maxEvEver = this.ruleCreationFormGroup.get('repetitions').value.ever;
      }


      console.log(this.rule);
    }
  }
}

export const RewardsValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  let valid = 0;
  Object.values(formGroup.controls).forEach(control => {
    if (control.value) {
      valid += control.value.length;
    }
  });
  return valid === 0 ? {rewardsNotSelected: true} : null;
};

export const TemporalValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const validFrom = control.value.validFrom;
  const validTo = control.value.validTo;
  const now = new Date();
  let validFromDate: Date;
  let validToDate: Date;
  if (validTo) {
    validToDate = new Date(validTo);
    if (validToDate.getTime() < now.getTime()) {
      return {validToBeforeToday: true};
    }
  }
  if (validFrom) {
    validFromDate = new Date(validFrom);
  }
  if (validFromDate && validToDate) {
    if (validFromDate.getTime() > validToDate.getTime()) {
      return {validToBeforeValidFrom: true};
    }
  }
  return null;
};

export const BeforeNowValidator: ValidatorFn = (control: FormControl): ValidationErrors | null => {
  if (control.value) {
    const now = new Date();
    const date = new Date(control.value);
    if (date.getTime() < now.getTime()) {
      return {beforeNow: true};
    }
  }
  return null;
};

export const RepetitionsValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const day = control.value.day;
  const ever = control.value.ever;
  if (day && ever) {
    if (day > ever) {
      return {day: 'greaterThanEver'};
    }
  }
  return null;
};


export function templateBuilder(): RuleTemplate[] {
  const templates = [
    new RuleTemplate('Add a picture', null, 'contribution_added', [new ObjectProperty('hasType', 'Photo')], []),
    new RuleTemplate('Add a 3D Object', null, 'contribution_added', [new ObjectProperty('hasType', '3D_Object')], []),
    new RuleTemplate('Add a Document', null, 'contribution_added', [new ObjectProperty('hasType', 'Document')], []),
    new RuleTemplate('Add a POI', null, 'contribution_added', [new ObjectProperty('hasType', 'Place')], []),
    new RuleTemplate('Add a Video', null, 'contribution_added', [new ObjectProperty('hasType', 'Video')], []),
    new RuleTemplate('Add a Comment', {referenceObjectParams: 'Select the type of object to comment'},
      'contribution_added', [new ObjectProperty('hasType', 'Comment')], []),
    new RuleTemplate('Add a Tag', null, 'contribution_added', [new ObjectProperty('hasType', 'Tag')], []),
    new RuleTemplate('Create an Activity', null, 'object_created', [new ObjectProperty('hasType', 'Event')], []),
    new RuleTemplate('Join an Activity', null, 'participation_added', [new ObjectProperty('hasType', 'Participation')], []),
    new RuleTemplate('Leave an Activity', null, 'participation_removed', [new ObjectProperty('hasType', 'Participation')], []),
    new RuleTemplate('Create a Task', null, 'object_created', [new ObjectProperty('hasType', 'Task')], []),
    new RuleTemplate('Check in a Task', null, 'task_checkin', [new ObjectProperty('hasType', 'TaskCheckInOut')], []),
    new RuleTemplate('Check out a Task', null, 'task_checkout', [new ObjectProperty('hasType', 'TaskCheckInOut')], []),
    new RuleTemplate('Create a Group', null, 'object_created', [new ObjectProperty('hasType', 'Group')], []),
    new RuleTemplate('Join a Group', null, 'membership_added', [new ObjectProperty('hasType', 'Membership')], []),
    new RuleTemplate('Leave a Group', null, 'membership_removed', [new ObjectProperty('hasType', 'Membership')], []),
    new RuleTemplate('Start an Initiative', null, 'object_created', [new ObjectProperty('hasType', 'Initiative')], []),
    new RuleTemplate('Support an Initiative', null, 'support_added', [new ObjectProperty('hasType', 'Initiative')], []),
    new RuleTemplate('Add Suggestion', null, 'object_created', [new ObjectProperty('hasType', 'Suggestion')], []),
    new RuleTemplate('Assess Suggestion', null, 'suggestion_rated', [], []),
    new RuleTemplate('Interest added', null, 'interest_added', [], []),
    new RuleTemplate('Interest removed', null, 'interest_removed', [], []),
    new RuleTemplate('Create a Coin', null, 'token_created', [new ObjectProperty('hasType', 'Coin')], []),
    new RuleTemplate('Spend a Coin', null, 'token_transferred', [], [new ObjectProperty('hasType', 'Coin')]),
    new RuleTemplate('Create a Coupon', null, 'token_created', [new ObjectProperty('hasType', 'Coupon')], []),
    new RuleTemplate('Spend a Coupon', null, 'token_transferred', [], [new ObjectProperty('hasType', 'Coupon')]),
  ];

  return templates;
}

export class RuleTemplate extends Rule {
  activity: string;
  hints: any = null;

  constructor(activity, hint, activityType, activityObjectProperties, referenceObjectProperties) {
    super();
    this.activity = activity;
    this.activity_type = activityType;
    this.activityObjectProperties = activityObjectProperties;
    this.referenceObjectProperties = referenceObjectProperties;
  }
}

