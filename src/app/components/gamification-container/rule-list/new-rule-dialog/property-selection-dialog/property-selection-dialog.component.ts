import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../rule-list.component';
import {OTMConcept, OTMProperty} from '../../../../../model/otmconcept';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ObjectProperty} from '../../../../../model/rule';

@Component({
  selector: 'app-property-selection-dialog',
  templateUrl: './property-selection-dialog.component.html',
  styleUrls: ['./property-selection-dialog.component.css']
})
export class PropertySelectionDialogComponent implements OnInit {
  concept: OTMConcept;
  newPropertyFormGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<PropertySelectionDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder) {
    this.concept = data.concept;
    console.log(data);
  }

  ngOnInit(): void {
    this.newPropertyFormGroup = this.fb.group({
      propertyName: new FormControl(null, [Validators.required]),
      propertyValue: new FormControl(null, [Validators.required])
    });
    this.newPropertyFormGroup.get('propertyName').valueChanges.subscribe(() => this.newPropertyFormGroup.get('propertyValue').reset());
  }

  getResponse(): ObjectProperty {
    if (!this.newPropertyFormGroup.get('propertyName').value) {
      return;
    }
    const propertyName = this.newPropertyFormGroup.get('propertyName').value.name;
    const propertyValue = this.newPropertyFormGroup.get('propertyValue').value;
    // switch (this.newPropertyFormGroup.get('propertyName').value.range){
    //   case 'Instant':
    //   case 'dateTimeStamp':
    //     const date = new Date(this.newPropertyFormGroup.get('propertyValue').value);
    //     propertyValue = date;
    //     break;
    //   default:
    //     propertyValue = this.newPropertyFormGroup.get('propertyValue').value;
    //     break;
    //
    // }
    return new ObjectProperty(propertyName, propertyValue);
  }
}
