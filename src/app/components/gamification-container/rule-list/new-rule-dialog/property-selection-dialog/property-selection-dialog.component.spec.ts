import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertySelectionDialogComponent } from './property-selection-dialog.component';

describe('PropertySelectionDialogComponent', () => {
  let component: PropertySelectionDialogComponent;
  let fixture: ComponentFixture<PropertySelectionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertySelectionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertySelectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
