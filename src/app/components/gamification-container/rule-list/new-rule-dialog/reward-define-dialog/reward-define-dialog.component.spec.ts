import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardDefineDialogComponent } from './reward-define-dialog.component';

describe('RewardDefineDialogComponent', () => {
  let component: RewardDefineDialogComponent;
  let fixture: ComponentFixture<RewardDefineDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RewardDefineDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardDefineDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
