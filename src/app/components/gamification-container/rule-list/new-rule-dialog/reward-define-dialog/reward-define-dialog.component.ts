import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ObjectProperty, Variable} from '../../../../../model/rule';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-reward-define-dialog',
  templateUrl: './reward-define-dialog.component.html',
  styleUrls: ['./reward-define-dialog.component.css']
})
export class RewardDefineDialogComponent implements OnInit {
  listVariable: string[];
  newRewardFormGroup: FormGroup;
  variablesFilteredOptions: Observable<string[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
              public dialogRef: MatDialogRef<RewardDefineDialogComponent>) {
    this.listVariable = data.listVariable;
  }

  ngOnInit(): void {
    this.newRewardFormGroup = this.fb.group({
      name: new FormControl(null, [Validators.required]),
      assignment: new FormControl(null, [Validators.required])
    });
    this.variablesFilteredOptions = this.newRewardFormGroup
      .get('name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterVariable(value))
    );
  }

  private _filterVariable(value: string): any[] {
    const filterValue = value ? value.toLowerCase() : null;
    if (!filterValue) {
      return this.listVariable;
    }
    return this.listVariable.filter(variable => variable.toLowerCase().indexOf(filterValue) === 0);
  }

  getResponse(): Variable {
    let name = this.newRewardFormGroup.get('name').value;
    if (name) {
      name = (name as string).toLowerCase();
    }
    const assignment = this.newRewardFormGroup.get('assignment').value;
    return new Variable(name, assignment);
  }
}
