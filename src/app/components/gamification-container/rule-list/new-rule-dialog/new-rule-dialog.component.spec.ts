import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRuleDialogComponent } from './new-rule-dialog.component';

describe('NewRuleDialogComponent', () => {
  let component: NewRuleDialogComponent;
  let fixture: ComponentFixture<NewRuleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRuleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRuleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
