import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ContainerComponent} from './components/container/container.component';
import {GamificationContainerComponent} from './components/gamification-container/gamification-container.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {RuleListComponent} from './components/gamification-container/rule-list/rule-list.component';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmDialogComponent} from './components/confirm-dialog/confirm-dialog.component';
import {BadgeListComponent} from './components/gamification-container/badge-list/badge-list.component';
import {BadgeCardComponent} from './components/gamification-container/badge-list/badge-card/badge-card.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {OrgChartModule} from 'angular-org-chart';
import {BadgePreconditionsDialogComponent} from './components/gamification-container/badge-list/badge-preconditions-dialog/badge-preconditions-dialog.component';
import {NewBadgeDialogComponent} from './components/gamification-container/badge-list/new-badge-dialog/new-badge-dialog.component';
import {RankingComponent} from './components/gamification-container/ranking/ranking.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BadgeStepComponent} from './components/gamification-container/badge-list/new-badge-dialog/badge-step/badge-step.component';
import {BadgePositionComponent} from './components/gamification-container/badge-list/new-badge-dialog/badge-position/badge-position.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatStepperModule} from '@angular/material/stepper';
import {BadgeLogosGalleryComponent} from './components/gamification-container/badge-list/new-badge-dialog/badge-logos-gallery/badge-logos-gallery.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {NewRuleDialogComponent} from './components/gamification-container/rule-list/new-rule-dialog/new-rule-dialog.component';
import {PropertySelectionDialogComponent} from './components/gamification-container/rule-list/new-rule-dialog/property-selection-dialog/property-selection-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatSortModule} from '@angular/material/sort';
import {RewardDefineDialogComponent} from './components/gamification-container/rule-list/new-rule-dialog/reward-define-dialog/reward-define-dialog.component';
import {GamificationLogListComponent} from './components/gamification-container/gamification-log-list/gamification-log-list.component';
import {RulePreviewComponent} from './components/gamification-container/rule-list/new-rule-dialog/rule-preview/rule-preview.component';
import {RuleDialogComponent} from './components/gamification-container/gamification-log-list/rule-dialog/rule-dialog.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import { BadgePreviewComponent } from './components/gamification-log-list/badge-preview/badge-preview.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AcaStatsComponent } from './components/gamification-container/aca-stats/aca-stats.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ContainerComponent,
    GamificationContainerComponent,
    RuleListComponent,
    ConfirmDialogComponent,
    BadgeListComponent,
    BadgeCardComponent,
    BadgePreconditionsDialogComponent,
    NewBadgeDialogComponent,
    RankingComponent,
    BadgeStepComponent,
    BadgePositionComponent,
    BadgeLogosGalleryComponent,
    NewRuleDialogComponent,
    PropertySelectionDialogComponent,
    RewardDefineDialogComponent,
    GamificationLogListComponent,
    RulePreviewComponent,
    RuleDialogComponent,
    BadgePreviewComponent,
    AcaStatsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTabsModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    MatExpansionModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    OrgChartModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    DragDropModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatProgressBarModule,
    MatSnackBarModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    MatChipsModule,
    MatMenuModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatProgressSpinnerModule,
    NgxChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/locales/', '.json');
}
