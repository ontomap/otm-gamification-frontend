import {BadgeContainer} from './badge-container';
import { v4 as uuid } from 'uuid';

export class VariablePrecondition {
  variable: string;
  threshold: number;
}

class BadgePrecondition {
  badgeList: string[];
  variables: VariablePrecondition[];

  constructor() {
    this.variables = [];
    this.badgeList = [];
  }
}

export class Badge{
  id: string;
  badgeContainer: BadgeContainer<Badge>;
  name: string;
  removable: boolean;
  active: boolean;
  logo: string;

  constructor() {
    this.id = uuid();
    this.removable = true;
    this.active = true;
  }

}

export class BadgeStep extends Badge{
  preconditions: BadgePrecondition;

  constructor() {
    super();
    this.preconditions = new BadgePrecondition();
  }

  static fromJson(json: any): BadgeStep {
    const badge = new BadgeStep();
    badge.id = json.id;
    badge.name = json.name;
    badge.active = json.active;
    badge.logo = json.logo;
    badge.preconditions.badgeList = json.preconditions.badgeList;
    json.preconditions.variables.forEach(pv => {
      const precVar = new VariablePrecondition();
      precVar.variable = pv.variable;
      precVar.threshold = pv.threshold;
      badge.preconditions.variables.push(precVar);
    });
    return badge;
  }
}

export class BadgePosition extends Badge{
  position: number;

  constructor() {
    super();
  }

  static fromJson(json: any): BadgePosition {
    const badge = new BadgePosition();
    badge.id = json.id;
    badge.name = json.name;
    badge.active = json.active;
    badge.logo = json.logo;
    badge.position = json.position;
    return badge;
  }
}
