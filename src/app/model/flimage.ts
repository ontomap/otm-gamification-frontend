export class FLImage {
  '_id': string;
  originalname: string;
  userId: string;
  url: string;
}
