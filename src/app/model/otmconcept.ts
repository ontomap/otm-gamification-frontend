export class OTMConcept {
  id: string;
  uri: string;
  propertyList: OTMProperty[];
}


export class OTMProperty {
  name: string;
  range: string;
  value: string;
}
