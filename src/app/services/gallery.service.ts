import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ACA} from '../model/aca';
import {map} from 'rxjs/operators';
import {FLImage} from '../model/flimage';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  constructor(private http: HttpClient) {
  }

  getImages(): Observable<FLImage[]> {
    return this.http.get(environment.FL_STORAGE + '/topic/' + environment.FIRSTLIFE_IMAGE_TAG, {
      headers: {
        authentication_server: environment.USER_AUTH.auth_server,
        Authorization : 'Bearer ' + environment.USER_AUTH.access_token
      }
    }).pipe(
      map((response: any) => {
        return response.flatMap(image => {
          image.url = environment.FL_STORAGE + '/files/' + image._id;
          return image;
        });
      })
    );
  }

  uploadImage(imageFile: File): Observable<any>{
    const formData = new FormData();
    formData.append('files', imageFile);
    return this.http.post(environment.FL_STORAGE + '/files', formData, {
      reportProgress: true,
      headers: {
        authentication_server: environment.USER_AUTH.auth_server,
        Authorization : 'Bearer ' + environment.USER_AUTH.access_token,
        topic: environment.FIRSTLIFE_IMAGE_TAG,
        status: 'public'
      }
    });
}
}
