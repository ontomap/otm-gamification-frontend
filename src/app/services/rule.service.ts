import {Injectable} from '@angular/core';
import {Rule} from '../model/rule';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {BadgeContainer} from '../model/badge-container';

@Injectable({
  providedIn: 'root'
})
export class RuleService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getRules(): Observable<Rule[]> {
    return this.http.get<Rule[]>(this.OTM_INSTANCE + '/api/v1/gamification/rules').pipe(
      map((response: any) => {
        const rules = [];
        response.forEach(bc => {
          rules.push(Rule.fromJson(bc));
        });
        return rules;
      })
    );
  }

  changeActive(id: string, flag: boolean): Observable<HttpResponse<any>> {
    return this.http.post<HttpResponse<any>>(this.OTM_INSTANCE + '/api/v1/gamification/rules/' + id + '?enable=' + flag, null, {
      headers: {
        Authorization: 'Bearer ' + environment.USER_AUTH.access_token
      }
    });
  }

  saveRule(rule: Rule): Observable<any> {
    return this.http.post(this.OTM_INSTANCE + '/api/v1/gamification/rules', [rule],
      {
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        }
      });
  }
}
