import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {ACA} from '../model/aca';

@Injectable({
  providedIn: 'root'
})
export class AcaService {

  constructor(private http: HttpClient) {
  }

  getAcas(): Observable<ACA[]> {
    return this.http.get(environment.FL_INSTANCE + '/v6/fl/Things/search?types=CO3_ACA').pipe(
      map((response: any) => {
       return response.things.features.flatMap(feature => {
         const aca = new ACA();
         aca.name = feature.properties.name;
         aca.url = feature.self;
         return aca;
       });
      })
    );
  }

  getAca(url: string): Observable<ACA> {
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.map(feature => {
          const aca = new ACA();
          aca.name = feature.properties.name;
          aca.url = feature.self;
          return aca;
        });
      })
    );
  }
}
