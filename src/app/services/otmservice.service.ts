import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {OTMConcept} from '../model/otmconcept';

@Injectable({
  providedIn: 'root'
})
export class OTMService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getActivityTypes(): Observable<string[]> {
    return this.http.get<string[]>(this.OTM_INSTANCE + '/api/v1/logger/events/activity_types');
  }

  getConceptList(): Observable<string[]> {
    return this.http.get<string[]>(this.OTM_INSTANCE + '/api/v1/concepts')
      .pipe(
        map((response: any) => {
          return response.concepts.flatMap(concept => {
            return concept.id;
          }).sort();
        })
      );
  }

  getConceptProperties(idConcept: string): Observable<OTMConcept> {
    return this.http.get<OTMConcept>(this.OTM_INSTANCE + '/api/v1/concepts/' + idConcept);
  }


}
