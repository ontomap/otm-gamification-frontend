import { TestBed } from '@angular/core/testing';

import { AcaProfileService } from './aca-profile.service';

describe('AcaProfileService', () => {
  let service: AcaProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcaProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
