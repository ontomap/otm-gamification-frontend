import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GamificationLog} from '../model/gamification-log';
import {PaginatedQueryResult} from '../model/paginated-query-result';

@Injectable({
  providedIn: 'root'
})
export class GamificationLogService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getLogs(aca: string, rules: boolean, badges: boolean, startTime: Date, endTime: Date, page: number, pageSize: number): Observable<PaginatedQueryResult<GamificationLog>> {
    return this.http.get<GamificationLog[]>(this.OTM_INSTANCE + '/api/v1/gamification/admin/logs',
      {
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        },
        params: {
          resolve: 'true',
          aca: aca,
          badges: badges + '',
          rules: rules + '',
          page: page.toString(),
          pageSize: pageSize.toString(),
          startTime: startTime ? startTime.getTime().toString() : '-1',
          endTime: endTime ? endTime.getTime().toString() : '-1',
        }
      }).pipe(
      map((response: any) => {
        const ret = new PaginatedQueryResult<GamificationLog>();
        ret.page = response.page;
        ret.pageSize = response.pageSize;
        ret.totalCount = response.totalCount;
        ret.results = [];
        response.results.forEach(log => {
          ret.results.push(GamificationLog.fromJson(log));
        });
        return ret;
      })
    );
  }
}
