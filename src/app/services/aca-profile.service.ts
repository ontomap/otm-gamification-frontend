import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ACA} from '../model/aca';
import {AcaProfile} from '../model/aca-profile';
import {environment} from '../../environments/environment';
import {BadgeContainer} from '../model/badge-container';
import {Badge} from '../model/badge';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AcaProfileService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getAcaProfile(aca: string): Observable<AcaProfile> {
    return this.http
      .get<AcaProfile>(this.OTM_INSTANCE + '/api/v1/gamification/profile/aca',{
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        },
        params: {
          aca
        }
      })
      .pipe(
        map((response: any) => {
          return  response ? AcaProfile.fromJson(response) : null;
        })
      );

  }
}
