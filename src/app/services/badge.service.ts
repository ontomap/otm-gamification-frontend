import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BadgeContainer} from '../model/badge-container';
import {Badge} from '../model/badge';
import {map} from 'rxjs/operators';
import {ACA} from '../model/aca';

@Injectable({
  providedIn: 'root'
})
export class BadgeService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getBadges(): Observable<BadgeContainer<Badge>[]> {
    return this.http
      .get<BadgeContainer<Badge>[]>(this.OTM_INSTANCE + '/api/v1/gamification/badgeContainers')
      .pipe(
        map((response: any) => {
          const bcList = [];
          response.forEach(bc => {
            bcList.push(BadgeContainer.fromJson(bc));
          });
          return bcList;
        })
      );
  }

  enableBadge(badge: Badge, enable: boolean): Observable<any> {
    return this.http.post(this.OTM_INSTANCE + '/api/v1/gamification/badges/' + badge.id + '?enable=' + enable,
      null,
      {
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        }
      });
  }

  saveBadgeContainer(badgeContainer: BadgeContainer<Badge>): Observable<any> {
    return this.http.post(this.OTM_INSTANCE + '/api/v1/gamification/badgeContainers', [badgeContainer],
      {
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        }
      });
  }

  updateBadgeContainer(badgeContainer: BadgeContainer<Badge>): Observable<any> {
    return this.http.patch(this.OTM_INSTANCE + '/api/v1/gamification/badgeContainers', [badgeContainer],
      {
        headers: {
          Authorization: 'Bearer ' + environment.USER_AUTH.access_token
        }
      });
  }


}
